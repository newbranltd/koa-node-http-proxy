// koa-node-http-proxy main interface
const httpProxy = require('http-proxy');
const {
  checkOptions
} = require('./lib');
const debug = require('debug')('koa-node-http-proxy:main');
const { inspect } = require('util');

/**
 * @param {object} config options
 * @param {array|object} config.proxies list of proxies or just one
 * @return {function} middleware
 */
module.exports = function(config) {
  const opt = checkOptions(config);
  return async function(ctx, next) {
    debug(inspect(ctx, false, null));
    await next();
  }
};
