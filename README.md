# koa-node-http-proxy

A Koa 2+ Middleware that wrap the node-http-proxy, using just a pass through no fancy option. The main thing is - IT SUPPORTS SOCKET PROXY, which is what I need.

## Installation

```sh
$ npm install koa-node-http-proxy
```

## Configuration

There are two ways to pass configurations, simple just an array or destinations you want to proxy.
Or another way to pass an object with some global modifications.  

### Quick and Simple, pass an array

```js
const app = new Koa();
app.use(KoaNodeHttpProxy([
  {
    host: 'http://somewherelese.com',
    url: 'somewherelese'
  }
]));
app.listen(3000);
```

When your call to `http://localhost/somewhereelse` it will get forward to `http://somewherelese.com/somewherelese`.

---

To proxy your socket connection

```js
app.use(KoaNodeHttpProxy([
  {
    host: 'http://somewherelese.com',
    ws: true,
    url: 'somewhere' // namespace!
  }
]));
```

Just pass the `ws:true` option, also the url become namespace (currently only support namespace forwarding).

---

MIT

[NEWBRAN LTD](https://newbran.ch) (c) 2019 / Joel Chu
