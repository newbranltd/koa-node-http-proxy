// start up the server
const Koa = require('koa');
const bodyparser = require('koa-bodyparser');
const debug = require('debug')('koa-node-http-proxy:helper:server');
const { inspect } = require('util');

module.exports = function(config = {}) {
  const app = new Koa();
  app.use(bodyparser());
  app.use(async function(ctx, next) {
    console.log(inspect(ctx, false, null));
    ctx.body = 'wtf';
    // await next();
  });

  return app;
}
