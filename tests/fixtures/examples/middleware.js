
const Koa = require('koa');
const app = new Koa();
const proxyMiddleware = require('../../../index');

app.use(proxyMiddleware({
  proxies: {
    ws: true,
    target: 'http://localhost:9015'
  }
}));

app.listen(6666);
