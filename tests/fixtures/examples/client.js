// socket.io client
const socketIoClient = require('socket.io-client');

const io = socketIoClient('http://localhost:8015');

io.on('msg', function(data) {
  console.log('got data: ', data);

  io.emit('reply', `Reply to your ${data.toString()}`);

  setTimeout(function() {
    io.emit('reply', 'I send this a bit later');
  }, 3000);
});
