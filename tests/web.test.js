// proxyServer.web test
const test = require('ava');
const request = require('superkoa');
const server = require('./helpers/server');

test.before(t => {
  t.context.app = server();
});

test.after(t => {

});

test('Just try to server to look at ctx', async t => {
  const res = await request(t.context.app).get('/');

  t.is(200, res.status);
});


test.todo('It should able to return content via the proxy');

test.todo('The errorHandler should able to handle error when proxy is not found');

test.todo('It should able to pass POST or PUT to the server behind proxy');
