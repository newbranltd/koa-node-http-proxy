// main export
const checkOptions = require('./check-options.js');
const createProxy = require('./create-proxy.js');

module.exports = {
  checkOptions,
  createProxy
};
