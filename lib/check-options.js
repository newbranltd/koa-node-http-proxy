/**
configuration options

- proixes (Array|object) { target: string
                           ws: boolean optional}

**/


/**
 * Check if the options is what we are looking for
 * @param {object} config configuration
 * @return {object} for correct or just throw
 */
module.exports = function(config) {
  
  if (config.proxies && (Array.isArray(config.proxies) || typeof config.proxies === 'object')) {
    config.proxies = Array.isArray(config.proxies) ? config.proxies : [config.proxies];
    // @TODO next check individual options
    return config;
  }
  throw new Error('proxies is required!');
}
